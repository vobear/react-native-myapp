/*
 * @Descripttion: 自定义标题栏
 * @Author: daxiong
 * @Date: 2020-10-20 13:27:05
 * @LastEditors: daxiong
 * @LastEditTime: 2020-10-28 16:51:48
 * @FilePath: e:\rn-dev\react-native-myapp\src\component\TitleBar\index.js
 */
import React, {Component} from 'react';
import {View, Text, Image, TouchableOpacity, StatusBar} from 'react-native';
import {scaleSize} from '../../utils/ScreenUtils';
import {
  css,
  border,
  borderB,
  borderT,
  padding,
  paddingLR,
  paddingTB,
  paddingT,
  paddingR,
  paddingB,
  paddingL,
  marginLR,
  marginTB,
  marginT,
  marginR,
  marginB,
  marginL,
  fontSize,
  bdRadius,
} from '../../utils/BaseStyle';
import Button from '../Button';

export default class TitleBar extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  static defaultProps = {
    title: '默认标题',
  };

  componentDidMount() {}

  //回退事件
  TitleBarBtnEvent = () => {
    if (typeof this.props.backEvent === 'function') {
      this.props.backEvent();
      return;
    }
    this.props.navigation.goBack();
  };

  render() {
    const {title} = this.props;
    return (
      <View
        style={[
          css.flexVH,
          css.flexRow_ssb,
          {width: '100%', height: scaleSize(90), backgroundColor: '#ffffff'},
        ]}>
        {/* <TouchableOpacity
          activeOpacity={1}
          style={[css.flexRow_cc, paddingLR(30), {height: '100%'}]}
          onPress={this.TitleBarBtnEvent}>
          <View style={[{width: scaleSize(24), height: scaleSize(36)}]}>
            <Image
              style={[css.wh100]}
              source={require('../../image/common/back_black.png')}
            />
          </View>
          <Text style={[css.c3, paddingL(16), fontSize(30)]}>{title}</Text>
        </TouchableOpacity> */}
        {/* <StatusBar backgroundColor="white" /> */}
        <View style={[css.flexRow_cc, {height: '100%'}]}>
          <TouchableOpacity
            activeOpacity={1}
            style={[
              css.flexVH,
              {
                width: scaleSize(80),
                height: '100%',
              },
            ]}
            onPress={this.TitleBarBtnEvent}>
            <Image
              style={[{width: scaleSize(24), height: scaleSize(36)}]}
              source={require('../../image/common/back_black.png')}
            />
          </TouchableOpacity>
          <Text style={[css.c3, fontSize(30)]}>{title}</Text>
        </View>
        <View style={[]}></View>
      </View>
    );
  }
}
