/*
 * @Descripttion: 自定义底部弹出操作菜单
 * @Author: daxiong
 * @Date: 2020-08-24 14:13:05
 * @LastEditors: daxiong
 * @LastEditTime: 2020-09-21 15:26:01
 * @FilePath: _customer\src\components\ShowActionSheet\index.js
 */

import React, { Component } from "react";
import {
  View,
  Text,
  Animated,
  TouchableOpacity,
  FlatList,
  Modal,
} from "react-native";
import {
  css,
  border,
  borderB,
  borderT,
  borderL,
  borderR,
  padding,
  paddingLR,
  paddingTB,
  paddingT,
  paddingR,
  paddingB,
  paddingL,
  marginLR,
  marginTB,
  marginT,
  marginR,
  marginB,
  marginL,
  fontSize,
} from "../../utils/BaseStyle.js";
import { scaleSize } from "../Common/ScreenUtils";
import ViewUtils from "../Common/ViewUtils";

class ShowActionSheet extends Component {
  static defaultProps = {};
  constructor(props) {
    super(props);
    this.state = {
      isShow: false,
      itemList: ["item1", "item2", "item3"], //操作菜单，最多6个
      //item默认样式
      itemStyle: {
        fontSize: 14,
        fontWeight: "400",
        color: "#333333",
      },
      cancel: true, //取消按钮是否显示
      cancelText: "取消", //取消按钮默认文本
      //取消按钮默认样式
      cancelTextStyle: {
        fontSize: 14,
        fontWeight: "400",
        color: "#999999",
      },
      success: null, //操作按钮点击后的到外部的回调事件
    };
  }

  componentDidMount() {}

  close() {
    this.setState({
      isShow: false,
    });
  }

  open() {
    this.setState({
      isShow: true,
    });
  }

  show(res) {
    const _show = () => {
      if (res.itemList && Array.isArray(res.itemList)) {
        if (res.itemList.length > 5) {
          console.error("itemList Maximum 6");
          return;
        }
        if (res.itemList.length > 0) {
          this.state.itemList = res.itemList;
        }
      }
      if (res.success && typeof res.success === "function") {
        this.state.success = res.success;
      }

      this.setState({
        itemList: this.state.itemList,
      });
      this.open();
    };
    if (typeof res === "object") {
      if (JSON.stringify(res) === "{}") {
        // console.error('The show method parameter of the showModal component should not be an empty object.');
        _show();
        return;
      }
      if (Array.isArray(res)) {
        console.error(
          "The show method parameter of the showModal component should be an object."
        );
        return;
      }
      _show();
    } else {
      console.error(
        "The show method parameter of the showModal component should be an object."
      );
    }
  }

  //取消操作
  cancel() {
    this.close();
  }

  //action点击事件接收
  actionItemEvent(index) {
    if (this.state.success && typeof this.state.success === "function") {
      this.state.success({ tapIndex: index });
    }
    this.close();
  }

  render() {
    return (
      <Modal
        animationType={"slide"}
        transparent={true}
        visible={this.state.isShow}
      >
        <TouchableOpacity
          style={[
            css.flex1,
            css.flexX_e,
            css.pr,
            { backgroundColor: "rgba(0,0,0,0.3)" },
          ]}
          activeOpacity={1}
          onPress={() => {
            this.close();
          }}
        >
          <View style={[css.pr, { backgroundColor: "#fff" }]}>
            {this.renderActive()}
            {this.renderCancel()}
            {ViewUtils.isXBottomView("#FFFFFF")}
          </View>
        </TouchableOpacity>
      </Modal>
    );
  }

  //返回操作行
  renderActive() {
    const { itemList, itemStyle } = this.state;
    if (itemList && itemList.length > 0) {
      return itemList.map((item, index) => {
        return (
          <TouchableOpacity
            activeOpacity={0.6}
            key={index}
            style={[
              css.flexVH,
              borderB("#F5F5F5", 0.5),
              { height: scaleSize(90) },
            ]}
            onPress={() => {
              this.actionItemEvent(index);
            }}
          >
            <Text style={[fontSize(30), css.c3]}>{item}</Text>
          </TouchableOpacity>
        );
      });
    }
  }

  //返回取消行
  renderCancel() {
    let cancel = ["取消"];
    return cancel.map((item, index) => {
      return (
        <TouchableOpacity
          activeOpacity={0.6}
          key={index}
          style={[css.flexVH, { height: scaleSize(90) }]}
          onPress={() => {
            this.cancel();
          }}
        >
          <Text style={[fontSize(30), css.c3]}>{item}</Text>
        </TouchableOpacity>
      );
    });
  }
}

export default ShowActionSheet;
