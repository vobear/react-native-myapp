/*
 * @Descripttion: android/ios按钮封装
 * @Author: daxiong
 * @Date: 2020-10-12 17:27:07
 * @LastEditors: daxiong
 * @LastEditTime: 2020-10-12 18:32:53
 * @FilePath: _customer\src\components\Button\index.js
 */

import React, { Component } from "react";
import {
  Text,
  View,
  TouchableOpacity,
  TouchableNativeFeedback,
  Platform,
} from "react-native";

export default class Button extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    if (Platform.OS === "ios") {
      return this.renderIosButton();
    }
    return this.renderAndroidButton();
  }

  renderAndroidButton() {
    return (
      <TouchableNativeFeedback
        delayPressIn={0}
        background={TouchableNativeFeedback.SelectableBackground()} // eslint-disable-line new-cap
        {...this.props}
      >
        {this.props.children}
      </TouchableNativeFeedback>
    );
  }

  renderIosButton() {
    return (
      <TouchableOpacity {...this.props}>{this.props.children}</TouchableOpacity>
    );
  }
}
