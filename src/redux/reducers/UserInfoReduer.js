/*
 * @Descripttion: 文件描述
 * @Author: daxiong
 * @Date: 2020-10-28 12:01:05
 * @LastEditors: daxiong
 * @LastEditTime: 2020-10-28 14:51:02
 * @FilePath: e:\rn-dev\react-native-myapp\src\redux\reducers\UserInfoReduer.js
 */
import {combineReducers} from 'redux';
import {update_userInfo} from '../action/actionType';

let defaultUserInfo = {
  user: 'admin',
  psw: 'abc123',
};

function updateUserInfo(state = defaultUserInfo, action) {
  // console.log('state=>', state);
  // console.log('action=>', action);
  switch (action.type) {
    case update_userInfo:
      return (state = action.userInfo);
    default:
      return state;
  }
}

export default updateUserInfo
