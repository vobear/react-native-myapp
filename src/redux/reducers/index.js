/*
 * @Descripttion: 文件描述
 * @Author: daxiong
 * @Date: 2020-10-26 16:26:14
 * @LastEditors: daxiong
 * @LastEditTime: 2020-10-28 17:22:04
 * @FilePath: e:\rn-dev\react-native-myapp\src\redux\reducers\index.js
 */
import {combineReducers} from 'redux';
import {CHANGE_NAME, update_chatList} from '../action/actionType';
import updateUserInfo from './UserInfoReduer';
import loginStatus from './LoginReduer';

// 原始默认state
const defaultState = {
  userlist: [
    {
      id: '001',
      name: '路人甲',
      sex: '女',
      age: 27,
    },
    {
      id: '002',
      name: '路人乙',
      sex: '女',
      age: 31,
    },
    {
      id: '003',
      name: '路人丙',
      sex: '男',
      age: 45,
    },
  ],
};

const defaultChatList = ['默认聊天记录'];

function user(state = defaultState.userlist, action) {
  switch (action.type) {
    case CHANGE_NAME:
      return state.map((item) =>
        item.id === action.id ? {...item, name: action.name} : item,
      );
    default:
      return state;
  }
}

function updateChatList(state = defaultChatList, action) {
  // console.log('state=>', state);
  // console.log('action=>', action);
  switch (action.type) {
    case update_chatList:
      return (state = action.list);
    default:
      return state;
  }
}

export default combineReducers({
  user,
  updateChatList,
  loginStatus,
  updateUserInfo,
});
