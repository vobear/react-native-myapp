/*
 * @Descripttion: 文件描述
 * @Author: daxiong
 * @Date: 2020-10-28 13:19:51
 * @LastEditors: daxiong
 * @LastEditTime: 2020-10-28 13:28:27
 * @FilePath: e:\rn-dev\react-native-myapp\src\redux\reducers\LoginReduer.js
 */

import { combineReducers } from "redux";
import { login_status } from "../action/actionType";

const defaultLoginStatus = 'login_no';

function loginStatus(state = defaultLoginStatus, action) {
  switch (action.type) {
    case login_status:
      switch (state) {
        case 'login_no':
          break;
        case 'login_success':
          break;
        case 'login_error':
          break;
        default:
          break;
      }
      return (state = action.status);
    default:
      return state;
  }
}

export default loginStatus