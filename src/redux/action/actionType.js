/*
 * @Descripttion: 文件描述
 * @Author: daxiong
 * @Date: 2020-10-26 16:23:58
 * @LastEditors: daxiong
 * @LastEditTime: 2020-10-28 11:59:49
 * @FilePath: e:\rn-dev\react-native-myapp\src\redux\action\actionType.js
 */
export const CHANGE_NAME = 'CHANGE_NAME';

export const update_chatList = 'update_chatList';

export const login_status = 'login_status';

export const update_userInfo = 'update_userInfo';
