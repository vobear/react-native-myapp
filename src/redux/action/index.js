import {
  CHANGE_NAME,
  update_chatList,
  login_status,
  update_userInfo,
} from './actionType.js';

const changeName = (id, name) => ({type: CHANGE_NAME, id: id, name: name});

const updateChatList = (list) => ({type: update_chatList, list: list});

const updateLoginStatus = (status) => ({type: login_status, status: status});

const updateUserInfo = (userInfo) => ({
  type: update_userInfo,
  userInfo: userInfo,
});

export {changeName, updateChatList, updateLoginStatus, updateUserInfo};
