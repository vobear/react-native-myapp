/*
 * @Descripttion: 文件描述
 * @Author: daxiong
 * @Date: 2020-10-26 16:23:29
 * @LastEditors: daxiong
 * @LastEditTime: 2020-10-28 14:47:39
 * @FilePath: e:\rn-dev\react-native-myapp\src\redux\store\index.js
 */
import {createStore} from 'redux';
import {applyMiddleware, compose} from 'redux';
import AsyncStorage from '@react-native-community/async-storage';
import {createLogger} from 'redux-logger';
import {persistStore, persistReducer} from 'redux-persist';
// import storage from 'redux-persist/lib/storage';
// import autoMergeLevel2 from 'redux-persist/lib/stateReconciler/autoMergeLevel2';
import reducers from '../reducers';

/** 方式一： 以下使用会抛出错误：redux-persist failed to create sync storage. falling back to noop storage. **/
// const persistConfig = {
//   key: 'root',
//   storage: storage,
//   stateReconciler: autoMergeLevel2, // 查看 'Merge Process' 部分的具体情况
// };

// const myPersistReducer = persistReducer(persistConfig, reducers);

// const store = createStore(myPersistReducer);

// const persistor = persistStore(store);

// export {store, persistor};

/** 方式二： 解决以上错误 **/
const persistConfig = {
  key: 'root',
  // Storage 方式来自React Native
  storage: AsyncStorage,
  //白名单(保存特定的reducer)
  whitelist: ['loginStatus'],
  // 黑名单(不保存特定的reducer)
  blacklist: [],
};

//Persist 持久化
const persistedReducer = persistReducer(persistConfig, reducers);

// Redux: Store
const store = createStore(persistedReducer, applyMiddleware(createLogger()));

// Redux 持久化副本
let persistor = persistStore(store);

export {store, persistor};
