/*
 * @Descripttion: 文件描述
 * @Author: daxiong
 * @Date: 2020-09-24 11:42:00
 * @LastEditors: daxiong
 * @LastEditTime: 2020-09-25 10:15:59
 * @FilePath: e:\rn-dev\react-native-myapp\src\screen\Home\Home2.js
 */
import React, {Component} from 'react';
import {View, Text, Button} from 'react-native';

export default class Home2 extends Component {

  constructor(props) {
    super(props);
    this.params = this.props.navigation.state.params;
    if (this.params.routes) {
      this.params.routes = [...this.params.routes, ...['Home2']];
    } else {
      this.params.routes = ['Home2'];
    }
    this.state = {};
  }

  componentDidMount() {
    console.log('[Home2 params]=>', this.params);
  }

  goto = () => {
    this.props.navigation.push('Home3',this.params)
  };
  

  render() {
    return (
      <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
        <Button title="Home3跳转" onPress={this.goto} />
      </View>
    );
  }
}
