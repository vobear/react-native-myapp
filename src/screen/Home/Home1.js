/*
 * @Descripttion: 文件描述
 * @Author: daxiong
 * @Date: 2020-09-24 11:42:00
 * @LastEditors: daxiong
 * @LastEditTime: 2020-10-28 16:51:30
 * @FilePath: e:\rn-dev\react-native-myapp\src\screen\Home\Home1.js
 */
import React, {Component} from 'react';
import {View, Text, Button} from 'react-native';
import {css} from '../../utils/BaseStyle';
import {scaleSize} from '../../utils/ScreenUtils';
import TitleBar from '../../component/TitleBar';
import LoadingToast from '../../component/LoadingToast';
import ToastMsg from '../../utils/ToastMsg';
import {connect} from 'react-redux';
import {changeName,updateChatList} from '../../redux/action';

class Home1 extends Component {
  constructor(props) {
    super(props);
    this.params = this.props.navigation.state.params;
    if (this.params.routes) {
      this.params.routes = [...this.params.routes, ...['Home1']];
    } else {
      this.params.routes = ['Home1'];
    }
    this.state = {};
  }

  componentDidMount() {
    console.log('[Home1 params]=>', this.params);
  }

  goto = () => {
    // this.props.navigation.navigate('My');
    this.props.navigation.push('Home2', this.params);
  };

  backEvent = () => {
    this.props.navigation.goBack();
  };

   changeValue = () => {
    this.props.dispatch(changeName('002', 'Home1修改的值'));
  };

  render() {
    return (
      <View style={[css.flex1]}>
        <TitleBar
          title={'我是标题'}
          navigation={this.props.navigation}
          backEvent={this.backEvent}
        />
        {/* <Button title="Home2跳转" onPress={this.goto} /> */}
        <Button title="修改值" onPress={this.changeValue} />
      </View>
    );
  }
}

const mapStateToProps = (state) => ({
  user: state.user,
});

export default connect(mapStateToProps)(Home1);
