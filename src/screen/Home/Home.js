/*
 * @Descripttion: 文件描述
 * @Author: daxiong
 * @Date: 2020-09-24 11:42:00
 * @LastEditors: daxiong
 * @LastEditTime: 2020-10-28 11:30:16
 * @FilePath: e:\rn-dev\react-native-myapp\src\screen\Home\Home.js
 */
import React, {Component} from 'react';
import {View, Text, Button} from 'react-native';
import {connect} from 'react-redux';
import {StackActions, NavigationActions} from 'react-navigation';
import {css, paddingT, marginT} from '../../utils/BaseStyle';
import {
  changeName,
  updateChatList,
  updateLoginStatus,
} from '../../redux/action';

class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {
    console.log('[Home props]', this.props);
  }

  goto = () => {
    this.props.navigation.push('Home1', {routes: ['Home']});
  };

  changeValue = () => {
    // this.props.dispatch(changeName('002', '新值'));
    this.props.dispatch(
      updateChatList(['第1条', '第2条', '第3条', '第4条', '第5条', '第6条']),
    );
  };

  loginOut = () => {
    this.props.dispatch(updateLoginStatus('login_no'));
    this.props.navigation.reset(
      [NavigationActions.navigate({routeName: 'Login'})],
      0,
    );
  };

  render() {
    return (
      <View style={[css.flex1]}>
        <Button title="Home1跳转" onPress={this.goto} />
        <View style={[marginT(20)]}>
          <Button title="修改值" onPress={this.changeValue} />
        </View>
        <View style={[marginT(20)]}>
          <Button title="退出登录" onPress={this.loginOut} />
        </View>
        {this.renderList()}
        {this.renderChatList()}
        <Text>登录状态：{this.props.loginStatus}</Text>
      </View>
    );
  }

  renderList() {
    const {user} = this.props;
    let listView = [];
    user.map((item, index) => {
      listView.push(
        <View key={index}>
          <Text>{item.name}</Text>
        </View>,
      );
    });
    return listView;
  }

  renderChatList() {
    const {updateChatList} = this.props;
    let listView = [];
    updateChatList.map((item, index) => {
      listView.push(
        <View key={index}>
          <Text>{item}</Text>
        </View>,
      );
    });
    return listView;
  }
}

const mapStateToProps = (state) => ({
  user: state.user,
  updateChatList: state.updateChatList,
  loginStatus: state.loginStatus,
});

export default connect(mapStateToProps)(Home);
