/*
 * @Descripttion: 文件描述
 * @Author: daxiong
 * @Date: 2020-09-24 18:14:37
 * @LastEditors: daxiong
 * @LastEditTime: 2020-09-25 11:00:04
 * @FilePath: e:\rn-dev\react-native-myapp\src\screen\My\My3.js
 */
import React, {Component} from 'react';
import {View, Text,Button} from 'react-native';
import {StackActions,NavigationActions} from 'react-navigation';

export default class My3 extends Component {
  goto = () => {
    // this.props.navigation.goBack();
    // this.props.navigation.popToTop();
    const resetAction = StackActions.reset({
      index: 0,
      actions: [NavigationActions.navigate({routeName: 'Tabbar'})],
    });
    this.props.navigation.dispatch(resetAction);
  };

  componentDidMount(){
    console.log('[componentDidMount My3]',this.props)
  }

  render() {
    return (
      <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
        <Button title="最后一页" onPress={this.goto} />
      </View>
    );
  }
}