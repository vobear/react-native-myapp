/*
 * @Descripttion: 文件描述
 * @Author: daxiong
 * @Date: 2020-09-24 18:46:18
 * @LastEditors: daxiong
 * @LastEditTime: 2020-10-22 11:09:19
 * @FilePath: e:\rn-dev\react-native-myapp\src\screen\My\My1.js
 */
import React, {Component} from 'react';
import {
  View,
  Text,
  Button,
  Platform,
  TextInput,
  StyleSheet,
  ScrollView,
  KeyboardAvoidingView,
} from 'react-native';
import {scaleSize} from '../../utils/ScreenUtils';
import {
  css,
  border,
  borderB,
  borderT,
  padding,
  paddingLR,
  paddingTB,
  paddingT,
  paddingR,
  paddingB,
  paddingL,
  marginLR,
  marginTB,
  marginT,
  marginR,
  marginB,
  marginL,
  fontSize,
} from '../../utils/BaseStyle';
export default class My1 extends Component {
  goto = () => {
    // this.props.navigation.goBack();
    this.props.navigation.push('My2');
  };

  render() {
    return (
      <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
        <Button title="My2跳转" onPress={this.goto} />
      </View>
    );
  }
}
