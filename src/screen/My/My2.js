/*
 * @Descripttion: 文件描述
 * @Author: daxiong
 * @Date: 2020-09-24 18:46:18
 * @LastEditors: daxiong
 * @LastEditTime: 2020-10-26 15:05:16
 * @FilePath: e:\rn-dev\react-native-myapp\src\screen\My\My2.js
 */
import React, {Component} from 'react';
import {View, Text, Button} from 'react-native';
import {StackActions, NavigationActions} from 'react-navigation';

export default class My2 extends Component {
  goto = () => {
    // this.props.navigation.goBack();
    // this.props.navigation.push('Tabbar')
    // this.props.navigation.popToTop();
    // const pushAction = StackActions.replace({
    //   routeName: 'Tabbar',
    // });
    // this.props.navigation.dispatch(pushAction);

    const resetAction = StackActions.reset({
      index: 0,
      actions: [
        NavigationActions.navigate({routeName: 'My3', params: {ceshi: '测试'}}),
      ],
    });
    this.props.navigation.dispatch(resetAction);
  };

  
  componentDidMount(){
    console.log('[componentDidMount My2]',this.props)
  }


  render() {
    return (
      <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
        <Button title="My3跳转" onPress={this.goto} />
      </View>
    );
  }
}
