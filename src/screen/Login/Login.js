/*
 * @Descripttion: 文件描述
 * @Author: daxiong
 * @Date: 2020-10-28 10:37:00
 * @LastEditors: daxiong
 * @LastEditTime: 2020-10-28 17:25:24
 * @FilePath: e:\rn-dev\react-native-myapp\src\screen\Login\Login.js
 */
import React, {Component} from 'react';
import {View, Text, TextInput, TouchableOpacity} from 'react-native';
import {connect} from 'react-redux';
import {StackActions, NavigationActions} from 'react-navigation';
import {
  css,
  paddingT,
  paddingB,
  marginT,
  fontSize,
} from '../../utils/BaseStyle';
import {scaleSize} from '../../utils/ScreenUtils';
import {updateUserInfo, updateLoginStatus} from '../../redux/action';

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      user: '',
      psw: '',
    };
  }

  componentDidMount() {
    const {loginStatus} = this.props;
    console.log('[Login]', this.props);
    switch (loginStatus) {
      case 'login_success':
        this.props.dispatch(updateLoginStatus('login_success'));
        const resetAction = StackActions.reset({
          index: 0,
          actions: [NavigationActions.navigate({routeName: 'Tabbar'})],
        });
        this.props.navigation.dispatch(resetAction);
        break;
      default:
        break;
    }
  }

  loginIn = () => {
    const {updateUserInfo} = this.props;
    if (
      updateUserInfo.user === this.state.user &&
      updateUserInfo.psw === this.state.psw
    ) {
      this.props.dispatch(updateLoginStatus('login_success'));
      const resetAction = StackActions.reset({
        index: 0,
        actions: [NavigationActions.navigate({routeName: 'Tabbar'})],
      });
      this.props.navigation.dispatch(resetAction);
    }
  };

  render() {
    return (
      <View style={[css.flex1, css.flexVH, {backgroundColor: '#ffffff'}]}>
        {/* <Text style={[css.c3, fontSize(40), paddingB(20)]}>登录</Text> */}
        <View
          style={[
            {
              width: scaleSize(600),
              height: scaleSize(80),
              backgroundColor: '#eee',
            },
          ]}>
          <TextInput
            stytle={[css.wh100, css.c3, fontSize(28)]}
            placeholder="请输入账号"
            onChangeText={(text) => {
              this.setState({
                user: text,
              });
            }}
          />
        </View>
        <View
          style={[
            marginT(20),
            {
              width: scaleSize(600),
              height: scaleSize(80),
              backgroundColor: '#eee',
            },
          ]}>
          <TextInput
            stytle={[css.wh100, css.c3, fontSize(28)]}
            placeholder="请输入密码"
            onChangeText={(text) => {
              this.setState({
                psw: text,
              });
            }}
          />
        </View>
        <TouchableOpacity
          activeOpacity={1}
          onPress={this.loginIn}
          style={[
            css.flexVH,
            marginT(30),
            {
              width: scaleSize(600),
              height: scaleSize(80),
              backgroundColor: '#999999',
            },
          ]}>
          <Text style={[css.cf, fontSize(40)]}>登录</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const reduxPorps = (state) => ({
  updateUserInfo: state.updateUserInfo,
  loginStatus: state.loginStatus,
});

export default connect(reduxPorps)(Login);
