/*
 * @Descripttion: 文件描述
 * @Author: daxiong
 * @Date: 2020-09-24 13:33:19
 * @LastEditors: daxiong
 * @LastEditTime: 2020-09-24 13:33:22
 * @FilePath: e:\rn-dev\myapp\src\router\NavigationService.js
 */
import {NavigationActions} from 'react-navigation';

let navigator;
//ref传值
const setTopLevelNavigator = navigatorRef => {
    navigator = navigatorRef;
};
//页面跳转方法
const navigate = (routeName, params = {}) => {
    navigator.dispatch(
        NavigationActions.navigate({
            routeName,
            params,
        }),
    );
};
//页面goBack方法
const goBack = () => {
    navigator.dispatch(NavigationActions.back());
};
//获取当前路由
const getCurrentRoute = () => {
    let route = navigator.state.nav;
    while (route.routes) {
        route = route.routes[route.index];
    }
    return route;
};
//获取当前路由名称
const getCurrentRouteName = () => {
    return getCurrentRoute().routeName;
};

export default {
    navigate,
    setTopLevelNavigator,
    getCurrentRoute,
    getCurrentRouteName,
    goBack,
};
