/*
 * @Descripttion: 样式
 * @Author: daxiong
 * @Date: 2020-08-20 10:07:24
 * @LastEditors: daxiong
 * @LastEditTime: 2020-10-20 13:53:58
 * @FilePath: e:\rn-dev\react-native-myapp\src\utils\BaseStyle.js
 */
import { StyleSheet } from "react-native";
import { scaleSize, setSpText } from "./ScreenUtils";

export const css = StyleSheet.create({
  tc: {
    textAlign: "center",
  },
  tl: {
    textAlign: "left",
  },
  tr: {
    textAlign: "right",
  },
  fi: {
    fontStyle: "italic",
  },

  flex1: {
    flex: 1,
  },
  flexV_sb: {
    alignItems: "center",
    justifyContent: "space-between",
  },
  flexVH: {
    alignItems: "center",
    justifyContent: "center",
  },
  flexH: {
    justifyContent: "center",
  },
  flexV: {
    alignItems: "center",
  },
  flexRow: {
    flexDirection: "row",
  },
  flexCol: {
    flexDirection: "column",
  },
  flexWrap: {
    flexWrap: "wrap",
  },
  pr: {
    position: "relative",
  },
  pa: {
    position: "absolute",
  },
  vh: {
    overflow: "hidden",
  },
  flexRow_sb: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
  },
  flexRow_ssb: {
    flexDirection: "row",
    alignItems: "flex-start",
    justifyContent: "space-between",
  },
  flexRow_fs: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "flex-start",
  },
  flexRow_cw: {
    flexDirection: "row",
    alignItems: "center",
    flexWrap: "wrap",
  },
  flexRow_cc: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
  },
  flexRow_sa_c: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-around",
  },
  flexRow_ee: {
    flexDirection: "row",
    alignItems: "flex-end",
    justifyContent: "flex-end",
  },
  flexRow_es: {
    flexDirection: "row",
    alignItems: "flex-start",
    justifyContent: "flex-end",
  },
  flexRow_ec: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "flex-end",
  },
  flexRow_cs: {
    flexDirection: "row",
    alignItems: "flex-start",
    justifyContent: "center",
  },
  flexRow_fec: {
    flexDirection: "row",
    alignItems: "flex-end",
    justifyContent: "center",
  },
  flexRow_ss: {
    flexDirection: "row",
    alignItems: "flex-start",
    justifyContent: "flex-start",
  },
  flexX_s: {
    justifyContent: "flex-start",
  },
  flexX_e: {
    justifyContent: "flex-end",
  },
  flexX_sa: {
    justifyContent: "space-around",
  },
  flexX_c: {
    justifyContent: "center",
  },
  flexY_s: {
    alignItems: "flex-start",
  },
  flexY_e: {
    alignItems: "flex-end",
  },
  cf: {
    color: "#ffffff",
  },
  c3: {
    color: "#333333",
  },
  c34: {
    color: "#343434",
  },
  c9: {
    color: "#999999",
  },
  c6: {
    color: "#666666",
  },
  h100: {
    height: "100%",
  },
  w100: {
    width: "100%",
  },
  wh100: {
    height: "100%",
    width: "100%",
  },
  w50: {
    width: "50%",
  },
  lh1: {
    lineHeight: 1,
  },
  txtunderline: {
    textDecorationLine: "underline",
  },
  fb: {
    fontWeight: "bold",
  },
  fb400: {
    fontWeight: "400",
  },
  fb600: {
    fontWeight: "600",
  },
  bx: {
    width: "100%",
    height: "100%",
  },
});

export const fontSize = (num) => ({
  fontSize: setSpText(num),
});

export const marginT = (num) => ({
  marginTop: scaleSize(num),
});
export const marginR = (num) => ({
  marginRight: scaleSize(num),
});
export const marginB = (num) => ({
  marginBottom: scaleSize(num),
});
export const marginL = (num) => ({
  marginLeft: scaleSize(num),
});
export const marginLR = (num) => ({
  marginLeft: scaleSize(num),
  marginRight: scaleSize(num),
});
export const marginTB = (num) => ({
  marginTop: scaleSize(num),
  marginBottom: scaleSize(num),
});
export const paddingLR = (num) => ({
  paddingLeft: scaleSize(num),
  paddingRight: scaleSize(num),
});
export const paddingL = (num) => ({
  paddingLeft: scaleSize(num),
});
export const paddingR = (num) => ({
  paddingRight: scaleSize(num),
});
export const paddingTB = (num) => ({
  paddingTop: scaleSize(num),
  paddingBottom: scaleSize(num),
});
export const paddingT = (num) => ({
  paddingTop: scaleSize(num),
});
export const paddingB = (num) => ({
  paddingBottom: scaleSize(num),
});
export const padding = (num) => ({
  paddingTop: scaleSize(num),
  paddingRight: scaleSize(num),
  paddingBottom: scaleSize(num),
  paddingLeft: scaleSize(num),
});

export const bdRadius = (num) => ({
  borderRadius: scaleSize(num),
});

export const borderL = (color = "#e6e6e6", width = 1, Style = "solid") => ({
  borderLeftWidth: width,
  borderLeftColor: color,
  borderStyle: Style,
});

export const borderR = (color = "#e6e6e6", width = 1, Style = "solid") => ({
  borderRightWidth: width,
  borderRightColor: color,
  borderStyle: Style,
});

export const borderB = (color = "#e6e6e6", width = 1, Style = "solid") => ({
  borderBottomWidth: width,
  borderBottomColor: color,
  borderStyle: Style,
});

export const borderT = (color = "#e6e6e6", width = 1, Style = "solid") => ({
  borderTopWidth: width,
  borderTopColor: color,
  borderStyle: Style,
});

export const borderTB = (color = "#e6e6e6", width = 1, Style = "solid") => ({
  borderTopWidth: width,
  borderTopColor: color,
  borderStyle: Style,
  borderBottomWidth: width,
  borderBottomColor: color,
});

export const border = (color = "#e6e6e6", width = 1, Style = "solid") => ({
  borderColor: color,
  borderWidth: width,
  borderStyle: Style,
});

//向下的三角形
export const triangle_down = (
  LR_width = 15,
  T_width = 20,
  color: "#999999"
) => ({
  width: 0,
  height: 0,
  borderLeftWidth: scaleSize(LR_width),
  borderLeftColor: "transparent",
  borderRightWidth: scaleSize(LR_width),
  borderRightColor: "transparent",
  borderTopWidth: scaleSize(T_width),
  borderTopColor: color,
});
//向上的三角形
export const triangle_up = (LR_width = 15, B_width = 20, color: "#999999") => ({
  width: 0,
  height: 0,
  borderLeftWidth: scaleSize(LR_width),
  borderLeftColor: "transparent",
  borderRightWidth: scaleSize(LR_width),
  borderRightColor: "transparent",
  borderBottomWidth: scaleSize(B_width),
  borderBottomColor: color,
});
//向左的三角形
export const triangle_left = (
  TB_width = 15,
  R_width = 20,
  color: "#999999"
) => ({
  width: 0,
  height: 0,
  borderTopWidth: scaleSize(TB_width),
  borderTopColor: "transparent",
  borderRightWidth: scaleSize(R_width),
  borderRightColor: color,
  borderBottomWidth: scaleSize(TB_width),
  borderBottomColor: "transparent",
});
//向右的三角形
export const triangle_right = (
  TB_width = 15,
  L_width = 20,
  color: "#999999"
) => ({
  width: 0,
  height: 0,
  borderTopWidth: scaleSize(TB_width),
  borderTopColor: "transparent",
  borderLeftWidth: scaleSize(L_width),
  borderLeftColor: color,
  borderBottomWidth: scaleSize(TB_width),
  borderBottomColor: "transparent",
});

//向右的箭头
export const rightArrow = (
  color = "#666666",
  width = 20,
  height = 20,
  borderTopWidth = 4,
  borderRightWidth = 4
) => ({
  width: scaleSize(width),
  height: scaleSize(height),
  borderColor: color,
  borderTopWidth: scaleSize(2),
  borderRightWidth: scaleSize(2),
  transform: [{ rotate: "45deg" }],
});

export const shadow = (
  color = "#e6e6e6",
  x = 0,
  y = 0,
  opacity = 1,
  radius = 0
) => ({
  shadowColor: color,
  shadowOffset: { width: x, height: y },
  shadowOpacity: opacity,
  shadowRadius: radius,
});

export const head = (size = 48) => ({
  width: scaleSize(size),
  height: scaleSize(size),
  borderRadius: scaleSize(size),
  overflow: "hidden",
});
