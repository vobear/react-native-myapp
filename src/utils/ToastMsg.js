/*
 * @Descripttion: 文件描述
 * @Author: daxiong
 * @Date: 2020-10-22 11:00:10
 * @LastEditors: daxiong
 * @LastEditTime: 2020-10-22 11:04:39
 * @FilePath: e:\rn-dev\react-native-myapp\src\utils\ToastMsg.js
 */
import Toast from 'react-native-root-toast';
const ToastMsg = {
  show: (msg, durationSet) => {
    const messageParam = {
      duration: durationSet,
      position: Toast.positions.CENTER,
      shadow: true,
      animation: true,
      hideOnPress: false,
      delay: 0,
    };

    Toast.show(msg, messageParam);
  },
};

export default ToastMsg;
